var global_config={
    iw:720,
    w:0,
    h:0,
    rate:1
};
function init_rem(){
    var iw=720;
    var w=$(window).width();
    var h=$(window).height();
    var rate=iw/w;
    var irate= 625/rate;
    global_config['rate']=rate;
    var str='html{font-size:' + irate + '%' + '}';
    var $style=$('<style></style>').html(str);
    $('head').eq(0).append($style);
}

init_rem();
window.onresize=function(){  
    init_rem();
    changeFooterFixed();
}
jQuery(document).ready(function($) {
    changeFooterFixed();
});
function changeFooterFixed(){
    var h=$(window).height();
    var vh=$('.v-body').height();
    var o_footer_height=108;
    var n_footer_height=108/global_config['rate'];
    if(vh + n_footer_height<h){
        $('.v-footer').addClass('v-footer-fixed');
    }else{
        $('.v-footer').removeClass('v-footer-fixed');
    }
}
